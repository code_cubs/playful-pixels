document.addEventListener('DOMContentLoaded', () => {
    const pieces = document.querySelectorAll('.piece');
    const positions = [
        '0 0', '100px 0', '200px 0',
        '0 100px', '100px 100px', '200px 100px',
        '0 200px', '100px 200px', '200px 200px'
    ];

    const shuffledPositions = shuffleArray([...positions]);

    pieces.forEach((piece, index) => {
        piece.style.backgroundImage = 'url("jigsaw.jpg")';
        piece.style.backgroundPosition = shuffledPositions[index];
        piece.dataset.position = shuffledPositions[index];
        piece.addEventListener('click', () => {
            swapPieces(piece);
            checkWin();
        });
    });

    let selectedPiece = null;

    function swapPieces(piece) {
        if (selectedPiece) {
            const tempPosition = piece.dataset.position;
            piece.dataset.position = selectedPiece.dataset.position;
            selectedPiece.dataset.position = tempPosition;

            piece.style.backgroundPosition = piece.dataset.position;
            selectedPiece.style.backgroundPosition = selectedPiece.dataset.position;

            selectedPiece = null;
        } else {
            selectedPiece = piece;
        }
    }

    function checkWin() {
        const currentPositions = Array.from(pieces).map(piece => piece.dataset.position);
        fetch('/check', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ positions: currentPositions })
        })
        .then(response => response.json())
        .then(data => {
            if (data.solved) {
                document.getElementById('puzzle-container').style.display = 'none';
                const congratsMessage = document.createElement('div');
                congratsMessage.textContent = 'Congratulations! You solved the puzzle!';
                document.body.appendChild(congratsMessage);
            }
        })
        .catch(error => console.error('Error:', error));
    }
    

    function shuffleArray(array) {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
        return array;
    }
});

