package main

import (
    "encoding/json"
    "net/http"
)

type PuzzleState struct {
    Positions []string `json:"positions"`
}

var correctPositions = []string{
    "0 0", "100px 0", "200px 0",
    "0 100px", "100px 100px", "200px 100px",
    "0 200px", "100px 200px", "200px 200px",
}

func main() {
    fs := http.FileServer(http.Dir("./static"))
    http.Handle("/", fs)
    http.HandleFunc("/check", checkPuzzle)

    http.ListenAndServe(":8081", nil)
}

func checkPuzzle(w http.ResponseWriter, r *http.Request) {
    var state PuzzleState
    err := json.NewDecoder(r.Body).Decode(&state)
    if err != nil {
        http.Error(w, err.Error(), http.StatusBadRequest)
        return
    }

    solved := true
    for i, pos := range state.Positions {
        if pos != correctPositions[i] {
            solved = false
            break
        }
    }

    response := map[string]bool{"solved": solved}
    json.NewEncoder(w).Encode(response)
}

