import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import System.Random (randomRIO)
import Data.List (nub, (\\))

type Card = (Int, Bool, Bool) -- (number, isFlipped, isMatched)
type GameState = ([Card], Int, Int) -- (cards, firstFlippedIndex, secondFlippedIndex)

main :: IO ()
main = do
    cards <- generateCards
    play display bgColor fps (cards, -1, -1) drawGame handleEvent updateGame
  where
    display = InWindow "Memory Game" (800, 600) (100, 100)
    bgColor = makeColorI 0 0 0 255 -- Black background
    fps = 60


generateCards :: IO [Card]
generateCards = do
    let numbers = [1..8] ++ [1..8]
    shuffledNumbers <- shuffle numbers
    return [(n, False, False) | n <- shuffledNumbers]

shuffle :: [a] -> IO [a]
shuffle [] = return []
shuffle xs = do
    i <- randomRIO (0, length xs - 1)
    let (left, (a:right)) = splitAt i xs
    rest <- shuffle (left ++ right)
    return (a:rest)

drawGame :: GameState -> Picture
drawGame (cards, _, _) = pictures
    [ translate (-200) 250 $ scale 0.5 0.5 $ color white $ text "Memory Game" -- Heading
    , pictures $ zipWith drawCard [0..] cards
    ]


drawCard :: Int -> Card -> Picture
drawCard i (n, isFlipped, isMatched) =
    translate x y $ pictures [color cardColor $ rectangleSolid 80 120, if isFlipped || isMatched then translate (-20) (-10) $ scale 0.2 0.2 $ color black $ text (show n) else blank]
  where
    (x, y) = (fromIntegral ((i `mod` 4) * 100 - 150), fromIntegral ((i `div` 4) * (-150) + 150))
    cardColor
        | isMatched = makeColorI 255 192 203 255 -- pink
        | isFlipped = white
        | otherwise = greyN 0.5

handleEvent :: Event -> GameState -> GameState
handleEvent (EventKey (MouseButton LeftButton) Up _ mousePos) (cards, firstIdx, secondIdx)
    | isInsideGrid mousePos = -- Check if the click is inside the card grid
        if idx /= firstIdx && idx /= secondIdx
        then
            if firstIdx == -1
            then (flipCard cards idx, idx, secondIdx)
            else if secondIdx == -1
                 then (flipCard cards idx, firstIdx, idx)
                 else (cards, -1, -1)
        else (cards, firstIdx, secondIdx)
    | otherwise = (cards, firstIdx, secondIdx) -- Ignore clicks outside the grid
  where
    idx = getCardIndex mousePos
    flipCard cs i = take i cs ++ [(n, not isFlipped, isMatched)] ++ drop (i + 1) cs
      where (n, isFlipped, isMatched) = cs !! i
    isInsideGrid (x, y) = abs x <= 200 && abs y <= 300 -- Adjust these values based on your card grid size
handleEvent _ gameState = gameState

getCardIndex :: (Float, Float) -> Int
getCardIndex (x, y) = (round ((x + 150) / 100)) + (round ((150 - y) / 150)) * 4

updateGame :: Float -> GameState -> GameState
updateGame _ (cards, firstIdx, secondIdx)
    | firstIdx /= -1 && secondIdx /= -1 && firstIdx /= secondIdx =
        if fst3 (cards !! firstIdx) == fst3 (cards !! secondIdx)
        then (matchCards cards firstIdx secondIdx, -1, -1)
        else (unflipCards cards firstIdx secondIdx, -1, -1)
    | otherwise = (cards, firstIdx, secondIdx)
  where
    fst3 (x, _, _) = x
    
    matchCards cs i j = take i cs ++ [(n, True, True)] ++ drop (i + 1) (take j cs ++ [(m, True, True)] ++ drop (j + 1) cs)
      where (n, _, _) = cs !! i
            (m, _, _) = cs !! j

    unflipCards cs i j = take i cs ++ [(n, False, False)] ++ drop (i + 1) (take j cs ++ [(m, False, False)] ++ drop (j + 1) cs)
      where (n, _, _) = cs !! i
            (m, _, _) = cs !! j

matchCards :: [Card] -> Int -> Int -> [Card]
matchCards cs i j =
    take i cs ++ [(n, True, True)] ++ drop (i + 1) (take j cs ++ [(m, True, True)] ++ drop (j + 1) cs)
  where
    (n, _, _) = cs !! i
    (m, _, _) = cs !! j